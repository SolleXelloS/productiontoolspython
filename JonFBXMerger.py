import os.path, time
from shutil import copy2
from ProgressBar import *

def scanDirectory(pathname,fileExtToReturnList):
    filenameList = list()
    totalFiles = len(os.listdir(pathname))
    count = -1
    for entry in os.scandir(pathname):
        count += 1
        progressBar(count,totalFiles,"Scanning Directory for Files")
        if (entry.path.endswith(fileExtToReturnList)):
            filename = entry.name.replace(fileExtToReturnList, "")
            if(not filename in filenameList):
                filenameList.append(filename)
        elif (os.path.isdir(entry.path)):
            filenameList.append(x for x in scanDirectory(entry.path,fileExtToReturnList))
    return filenameList

def ReturnNameDatePathFromPath(pathname,fileExtToReturnList):
    filenameList = dict()
    # Build List of Directory's and sory in date created order First to Last
    directoryList = SortByDateCreated(pathname)

    totalFileCount = len(directoryList)
    count = -1;
    # Build Dictionary of texture files
    for entry in directoryList:
        count += 1
        progressBar(count,totalFileCount,'Scanning for FBX Files')
        if (entry.path.endswith(fileExtToReturnList)):
            filename = entry.name.replace(fileExtToReturnList, "")
            filenameList[filename] = [os.path.getctime(entry.path),entry.path]
        elif (os.path.isdir(entry.path)):
            filenameList = Merge(filenameList,ReturnNameDatePathFromPath(entry.path,fileExtToReturnList))
    return filenameList

# Python code to merge dict using a single
# expression
def Merge(dict1, dict2):
    res = {**dict1, **dict2}
    return res

def SortByDateCreated(pathname):
    sortedPathList = sorted(os.scandir(pathname), key=os.path.getmtime)
    return sortedPathList

def BuildKVPDatePath(path):
    KVP = [time.ctime(os.path.getctime(path)),path]
    return KVP

def ReturnFBXSetFromPath(path,ext):
    fbxSet = set(scanDirectory(path,ext))
    return fbxSet

def ReturnKVPTextureDatePathFromPath(path, ext):
    texturesDict = dict()
    return texturesDict

Master_Models_Folder = ""
while Master_Models_Folder == "" or not os.path.isdir(Master_Models_Folder):
    Master_Models_Folder = input("\nEnter Full Path to your Project FBX Folder: ")
masterFBXSet = ReturnFBXSetFromPath(Master_Models_Folder, ".fbx") # TODO Refactor rename
# Iterate over every file i

jonTexturesFolder = ""
while jonTexturesFolder == "" or not os.path.isdir(jonTexturesFolder):
    jonTexturesFolder = input("\nEnter Full Path to Jon's Models: ")

jonTexturesDict = ReturnNameDatePathFromPath(jonTexturesFolder, ".fbx")
differenceSet = jonTexturesDict.keys() ^ masterFBXSet

print('Difference Set Length = %s' % str(len(differenceSet)))

# Copy all files from Jon's to Textures
totalCount = len(jonTexturesDict.keys())
count = -1
for file in jonTexturesDict:
    count += 1
    progressBar(count,totalCount,"Copying Files from Server to Project")
    desPath = os.path.join(Master_Models_Folder,file + ".fbx")
    copy2(jonTexturesDict[file][1], desPath)
print('Complete')
input("Enter to exit...")
