from os import listdir, rename, remove,path
from os.path import isfile, join
from sys import exc_info
import glob

MAT_DIR = path.dirname(path.realpath(__file__))
print(MAT_DIR)
fileCounter = len(glob.glob1(MAT_DIR,"*.mat"))

MAT_FILES = [f for f in listdir(MAT_DIR) if isfile(join(MAT_DIR, f))]
counter = 0;
print(str(counter) + " of " + str(fileCounter) + " to be renamed")
try:
    for MAT_FILE in MAT_FILES:
        FULL_PATH = join(MAT_DIR, MAT_FILE)
        NEW_NAME = "DG_" + MAT_FILE
        NEW_PATH = join(MAT_DIR, NEW_NAME)
        if MAT_FILE.endswith(".meta"):
            remove(FULL_PATH)
            continue
        elif MAT_FILE.endswith(".mat"):
            rename(FULL_PATH, NEW_PATH)
            counter+=1
except:
    print("Unexpected error:", exc_info()[0])
    input("Press Enter to continue...")

else:
    print("Success "+ str(counter) + " of " + str(fileCounter) + " renamed")
    input("Press Enter to continue...")
