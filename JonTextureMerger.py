import os.path, time
from shutil import copy2

def scanDirectory(pathname,fileExtToReturnList):
    filenameList = list()
    for entry in os.scandir(pathname):
        if (entry.path.endswith(fileExtToReturnList)):
            filename = entry.name.replace(fileExtToReturnList, "")
            if(not filename in filenameList):
                filenameList.append(filename)
        elif (os.path.isdir(entry.path)):
            filenameList.append(x for x in scanDirectory(entry.path,fileExtToReturnList) not in filenameList)
    return filenameList

def ReturnNameDatePathFromPath(pathname,fileExtToReturnList):
    filenameList = dict()
    # Build List of Directory's and sory in date created order First to Last
    directoryList = SortByDateCreated(pathname)
    # Build Dictionary of texture files
    for entry in directoryList:
        if (entry.path.endswith(fileExtToReturnList)):
            filename = entry.name.replace(fileExtToReturnList, "")
            filenameList[filename] = [os.path.getctime(entry.path),entry.path]
        elif (os.path.isdir(entry.path)):
            print(entry.path + " is processing")
            filenameList = Merge(filenameList,ReturnNameDatePathFromPath(entry.path,fileExtToReturnList))
            # input("Folder Processed...")
    return filenameList

# Python code to merge dict using a single
# expression
def Merge(dict1, dict2):
    res = {**dict1, **dict2}
    return res

def SortByDateCreated(pathname):
    sortedPathList = sorted(os.scandir(pathname), key=os.path.getmtime)
    return sortedPathList

def BuildKVPDatePath(path):
    KVP = [time.ctime(os.path.getctime(path)),path]
    return KVP

def ReturnTextureSetFromPath(path,ext):
    texturesSet = set(scanDirectory(path,ext))
    return texturesSet

def ReturnKVPTextureDatePathFromPath(path, ext):
    texturesDict = dict()
    return texturesDict

Master_Textures_Folder = ""
while Master_Textures_Folder == "" or not os.path.isdir(Master_Textures_Folder):
    Master_Textures_Folder = input("\nEnter Full Path to your Project Textures: ")
masterTextureSet = ReturnTextureSetFromPath(Master_Textures_Folder, ".exr")
print(masterTextureSet)
print(str(len(masterTextureSet)) + " Textures in the set")
print(str(len(scanDirectory(Master_Textures_Folder,".exr"))) + " Textures in the Master Folder")

input("Enter to continue...")
# Iterate over every file i

jonTexturesFolder = ""
while jonTexturesFolder == "" or not os.path.isdir(jonTexturesFolder):
    jonTexturesFolder = input("\nEnter Full Path to Jon's Textures: ")

jonTexturesDict = ReturnNameDatePathFromPath(jonTexturesFolder, ".exr")
print(jonTexturesDict)
print(str(len(jonTexturesDict)) + " Textures in the Dictionary")
differenceSet = jonTexturesDict.keys() ^ masterTextureSet
print('Difference Set = %s ' % differenceSet)
print('Difference Set Length = %s' % str(len(differenceSet)))
input("Enter to continue...")

# Copy all files from Jon's to Textures

for file in jonTexturesDict:
    # print(file)
    # print(jonTexturesDict[file][1])
    # print('Copy to %s' % desPath)
    desPath = os.path.join(Master_Textures_Folder,file + ".exr")
    copy2(jonTexturesDict[file][1], desPath)
input("Enter to exit...")
