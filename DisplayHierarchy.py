"""

 Copyright (C) 2001 - 2010 Autodesk, Inc. and/or its licensors.
 All Rights Reserved.

 The coded instructions, statements, computer programs, and/or related material 
 (collectively the "Data") in these files contain unpublished information 
 proprietary to Autodesk, Inc. and/or its licensors, which is protected by 
 Canada and United States of America federal copyright law and by international 
 treaties. 
 
 The Data may not be disclosed or distributed to third parties, in whole or in
 part, without the prior written consent of Autodesk, Inc. ("Autodesk").

 THE DATA IS PROVIDED "AS IS" AND WITHOUT WARRANTY.
 ALL WARRANTIES ARE EXPRESSLY EXCLUDED AND DISCLAIMED. AUTODESK MAKES NO
 WARRANTY OF ANY KIND WITH RESPECT TO THE DATA, EXPRESS, IMPLIED OR ARISING
 BY CUSTOM OR TRADE USAGE, AND DISCLAIMS ANY IMPLIED WARRANTIES OF TITLE, 
 NON-INFRINGEMENT, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OR USE. 
 WITHOUT LIMITING THE FOREGOING, AUTODESK DOES NOT WARRANT THAT THE OPERATION
 OF THE DATA WILL BE UNINTERRUPTED OR ERROR FREE. 
 
 IN NO EVENT SHALL AUTODESK, ITS AFFILIATES, PARENT COMPANIES, LICENSORS
 OR SUPPLIERS ("AUTODESK GROUP") BE LIABLE FOR ANY LOSSES, DAMAGES OR EXPENSES
 OF ANY KIND (INCLUDING WITHOUT LIMITATION PUNITIVE OR MULTIPLE DAMAGES OR OTHER
 SPECIAL, DIRECT, INDIRECT, EXEMPLARY, INCIDENTAL, LOSS OF PROFITS, REVENUE
 OR DATA, COST OF COVER OR CONSEQUENTIAL LOSSES OR DAMAGES OF ANY KIND),
 HOWEVER CAUSED, AND REGARDLESS OF THE THEORY OF LIABILITY, WHETHER DERIVED
 FROM CONTRACT, TORT (INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE), OR OTHERWISE,
 ARISING OUT OF OR RELATING TO THE DATA OR ITS USE OR ANY OTHER PERFORMANCE,
 WHETHER OR NOT AUTODESK HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH LOSS
 OR DAMAGE. 
 
"""

def DisplayHierarchy(pScene):
    lRootNode = pScene.GetRootNode()

    for i in range(lRootNode.GetChildCount()):
        DisplayNodeHierarchy(lRootNode.GetChild(i), 0)

def DisplayNodeHierarchy(pNode, pDepth):
    lString = ""
    for i in range(pDepth):
        lString += "     "

    lString += pNode.GetName()

    print(lString)

    for i in range(pNode.GetChildCount()):
        DisplayNodeHierarchy(pNode.GetChild(i), pDepth + 1)

def GetHierarchyAsList(pScene):
    outputList = []
    lRootNode = pScene.GetRootNode()
    for i in range(lRootNode.GetChildCount()):
        # print("Getting Hierarchy of %s" % lRootNode.GetName())
        lowerNodeSet = GetNodeHierarchyAsList(lRootNode.GetChild(i), 0)
        # print("Lower Node Set = %s " % lowerNodeSet)
        if(lowerNodeSet != None):
            # print("Adding to outputSet = %s " % outputSet)
            outputList.extend(lowerNodeSet)
            # print("Added to outputSet = %s " % outputSet)
    # print("returning outputSet = %s" %outputSet)
    return outputList

def GetNodeHierarchyAsList(pNode, pDepth):
    outputList = []
    # print("Adding %s node to set" % pNode.GetName())
    outputList.append(pNode.GetName())
    # print("Child count = %s " % pNode.GetChildCount())
    for i in range(pNode.GetChildCount()):
        lowerNodeSet = GetNodeHierarchyAsList(pNode.GetChild(i), pDepth + 1)
        # print("Lower Node Set = %s " % lowerNodeSet)
        if(lowerNodeSet != None):
            # print("Adding to outputSet = %s " % outputSet)
            outputList.extend(lowerNodeSet)
            # print("Added to outputSet = %s " % outputSet)
    # print("returning outputSet = %s" %outputSet)
    return outputList