import os

directoriesToMake = [
    # "DG",
    # "DG/Bakes",
    "_Vendor/Cars",
    "_Vendor/JSONs",
    "_Vendor/Materials",
    "_Vendor/Parts/FBX",
    "_Vendor/Textures",
    "DG/Bakes/json",
    "DG/Bakes/textures",
    # "DG/Data",
    "DG/Data/Regions",
    # "DG/Prefabs",
    "DG/Prefabs/Regions",
    # "DG/REQUESTS",
    "DG/REQUESTS/Regions",
    # "Resources",
    # "Resources/DG",
    # "Resources/Unity",
    # "Resources/Unity/Materials",
    "Resources/Unity/Materials/LOW",
    # "Resources/DG/Materials",
    # "Resources/DG/Materials/DebugView",
    "Resources/DG/Materials/DebugView/Bakes",
    "Resources/DG/Materials/LOW_DG",
]

for directory in directoriesToMake:
    try:
        os.makedirs(directory)
    except OSError:
        print("Creation of the directory %s has failed" % directory)
    else:
        print("Created Directory %s" % directory)

input("Press any button to Continue...")