import json
import os

JSON_PATH = ""
while JSON_PATH == "" or not os.path.isfile(JSON_PATH) or not JSON_PATH.endswith(".json"):
    JSON_PATH = input("\nEnter a Bake Set JSON file full path: ")

input_file = open(JSON_PATH)
output_path = JSON_PATH.replace(".json", "_pruned.json")

bake_list = json.load(input_file)

with open(output_path, "wb") as ext_file:
    json.dump(bake_list, ext_file, sort_keys=True, indent=4)

print("\nWrote to " + output_path + "!")
