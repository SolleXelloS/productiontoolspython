# from __future__ import print_function
import scandir
import datetime
import os, glob
import csv
from UtilitieFunctions import *
from ErrorHandling import ThrowError
from fbxUtilities import *
from ProgressBar import *

# {'all_errors':[{issues:{'error_type':[]}}]}
# {'all_errors':[{issues:{'error_type':[{'carpart_id':}]}}]}
RequestFileArrayReference = {}
carpartErrorsHighLevel = {}
#Log various errors found in script - duplicate_entry - missing_part - missing_subparts - missing_material
def LogError(requestFile,errorType,jsonObject,subpart = None, material = None):
    #TODO Validate JSON Object Structure
    if(not 'carpart' in jsonObject):
        print('No valid json passed in LogError')
    #Store Error into high level data structure for later printing
    if(not jsonObject['carpart'] in carpartErrorsHighLevel):
        carpartErrorsHighLevel[jsonObject['carpart']] = {} 
    if(not errorType in  carpartErrorsHighLevel[jsonObject['carpart']]):
         carpartErrorsHighLevel[jsonObject['carpart']][errorType] = {'count':0,'request_file':[]}
    carpartErrorsHighLevel[jsonObject['carpart']][errorType]['count']+=1
    if(not 'request_file' in carpartErrorsHighLevel[jsonObject['carpart']][errorType]):
        carpartErrorsHighLevel[jsonObject['carpart']][errorType]['request_file'] = ()
    if(not requestFile in carpartErrorsHighLevel[jsonObject['carpart']][errorType]['request_file']):
        carpartErrorsHighLevel[jsonObject['carpart']][errorType]['request_file'].append(requestFile)
    if(not subpart == None):
        carpartErrorsHighLevel[jsonObject['carpart']][errorType]['subparts'] = subpart
    if(not material == None):    
        carpartErrorsHighLevel[jsonObject['carpart']][errorType]['materials'] = material

def ReturnRequestEntry(requestFile):
    if (requestFile in RequestFileArrayReference):  
        return RequestFileArrayReference[requestFile]
    else:
        returnObject = {'request':requestFile,'issues':{}} 
        listMissingFiles['all_errors'].append(returnObject)
        RequestFileArrayReference[requestFile] = returnObject
        return returnObject


def IncreaseListID():
    global listID
    listID +=1
    return "%02d"%listID

def GenerateDate():
    today = datetime.date.today()
    datem = "{}/{}/{}".format(today.month, today.day, today.year)
    return datem

# Expected In JSON Objects Structures: Wheels, Brakes, Car Geometry: {carpart:int, subparts:[{subpart_ID:string, mat:string}]}
# Input: jsonobject - input JSON Object: 1 level up from subparts,subpart_ID,mat | Output: bool
def isValidJsonObject(jsonobject):
    if ('carpart' and 'subparts' in jsonobject):
        if(len(jsonobject['subparts']) > 0):
            if ('subpart_ID' and 'mat' in jsonobject['subparts'][0]):
                return True
    else:
        return False

# Input: jsonobject - input JSON Object: 1 level up from subparts,subpart_ID,mat , jsonKey - Key of JSON Object| Output: Dicitonary {carparts : [{carpart:string, subparts : [],mats:[]}]}
def BuildDictFromJSONObject(jsonObject, jsonKey):
    outputDict = dict()
    outputDict['carparts'] = []
    # Set the appending Key and iterate through each part
    for carpart in jsonObject[jsonKey]:
        appendedKey = {}
        if(isValidJsonObject(carpart)):
            appendedKey['carpart'] = carpart['carpart']
            appendedKey['subparts'] = BuildListFromSubpartJSONObject(carpart, 'subpart_ID')
            appendedKey['mats'] = BuildListFromSubpartJSONObject(carpart, 'mat')
            outputDict['carparts'].append(appendedKey)
        else:
            ThrowError('JSON Object not formatted properly not in %s' % carpart)
    return outputDict


# Expect rawJSON node in to be 1 level up from subparts node {jsonObject[jsonKey]: [{subparts:{}}]}
# Input: jsonobject - input JSON Object: 1 level up from subparts,subpart_ID,mat , jsonKey - Key of JSON Object , | Output: Dicitonary {Filename: [subpart_ID,mat]}
def BuildListFromJSONObject(jsonObject, jsonKey, keyToBuildSetFrom):
    # outputSet = set()
    outputList = []
    # For each wheel, brake and carpart build dictionary of carpart name and material/subpart_id
    for object in jsonObject[jsonKey]:
        if ('subparts' not in object):
            ThrowError('subparts not in %s' % object)
        else:
            subpartsDict = dict(object['subparts'][0])
            if (keyToBuildSetFrom not in subpartsDict.keys()):
                ThrowError('setToBuild not in %s' % subpartsDict.keys())
            else:
                outputList.append(subpartsDict[keyToBuildSetFrom])
    return outputList

# Input: jsonobject - input JSON Object: 1 level up from subparts,subpart_ID,mat , jsonKey - Key of JSON Object , | Output: Dicitonary {Filename: [subpart_ID,mat]}
def BuildListFromSubpartJSONObject(jsonObject, keyToBuildSetFrom):
    outputList = []
    # For each wheel, brake and carpart build dictionary of carpart name and material/subpart_id
    for object in jsonObject['subparts']:
        if (keyToBuildSetFrom not in object):
            print(keyToBuildSetFrom)
            ThrowError('setToBuild not in %s' % object)
        else:
            outputList.append(object[keyToBuildSetFrom])
    return outputList

# Iterate over directory files @ directoryPath and return [] of files with fileext
def GetListFromFolderOfExt(directoryPath, fileext):
    # Do we have a directory to start with?
    # Check Folder Directory Exists and has files and Extention has . prefix and folder
    if (not os.path.isdir(directoryPath)):
        return "Need valid Directory input"
    if (len(os.listdir(directoryPath)) == 0):
        return "Directory has no Files or Folders"
    if (fileext[0] != '.'):
        return "Not Valid Ext"
    outputList = []
    # Build List
    for entry in scandir.scandir(directoryPath):
        # If flie we search for then add to output
        if (entry.path.endswith(fileext)):
            filename = entry.name.replace(fileext, "")
            outputList.append(filename)
        # If Folder then iterate through folder for more files
        elif (os.path.isdir(entry.path)):
            directoryList = BuildDictFromFolderWithExt(entry.path, fileext)
            outputList.extend(directoryList)
    return outputList

# Iterate over files @ directoryPath and return Dict of {filename:[List of hierarchy]}
# Input: directoryPath | Output: {fbx filename: [List Hierarchy fbx file]}
def GenerateDictionaryFBXAndHierarchyFromFolder(directoryPath):
    outputDictionary = dict()
    count = 0
    totalFiles = len(os.listdir(directoryPath))
    for entry in scandir.scandir(directoryPath):
        count += 1
        
        progressBar(count,totalFiles,"Generating Dictionary from FBX's Hierarchy From Folder")
    # If flie we search for then add to output
        if (entry.path.endswith(".fbx")): #Generate Dict of Texture - mats and parts
            fbxName = entry.name.replace(".fbx","")
            outputDictionary[fbxName] = GetHierarchyListFromFBXFile(entry.path)
            # implement if you want list of parts, textures, and mats
            # outputDictionary[fbxName]['parts'] = GetHierarchyListFromFBXFile(entry.path)
            # outputDictionary[fbxName]['mats'] = GetMaterialListFromFBXFile(entry.path)
            # outputDictionary[fbxName]['textures'] = GetTexturesListFromFBXFile(entry.path)
    print("Finished")
    return outputDictionary

# Iterate over .json request files and return Dict {Request filename: [Wheelset, BrakeSet, CarGeometrySet]}
def GenerateDictionaryCarpartSubpart(requestsDirectoryPath):
    outputDictionary = dict()
    count = 0
    # print("total files to process = %s" % len(os.listdir(requestsDirectoryPath))) #TODO output length of files with .json
    # startProgress("Scanning Request Files")
    totalFiles = len(os.listdir(requestsDirectoryPath))
    #TODO Add glob to accuratly represent the files with valid ext to process and display
    for entry in scandir.scandir(requestsDirectoryPath):
        count += 1
        progressBar(count,totalFiles,"Parsing Request Files for parts")
        # progress(count/totalFiles)
        if (entry.path.endswith(".json")):
            requestName = entry.name.replace(".json","")
            outputDictionary[requestName] = {}
            f = open(entry.path)
            rawJSON = json.load(f)
            if ("WHEEL_SET" and "CAR_GEOMETRY" in rawJSON):  # Generate Sets if Wheelset and Car Geometry present
                # Build Dictionary of wheels - brakes - car geometry and check each subpart is in garage. {carpart:{name:string,subparts:[subparts]}}
                outputDictionary[requestName]['WHEEL_SET'] = BuildDictFromJSONObject(rawJSON["WHEEL_SET"], 'wheels')
                outputDictionary[requestName]['BRAKES_SET'] = BuildDictFromJSONObject(rawJSON["WHEEL_SET"], 'brakes')
                outputDictionary[requestName]['CAR_GEOMETRY'] = BuildDictFromJSONObject(rawJSON, 'CAR_GEOMETRY')
            else:
                print("JSON object not formatted with WHEEL_SET OR CAR_GEOMETRY")
    # endProgress()
    return outputDictionary

def FormatOutputFor(format):
    if(format == "json"):
        outputVar = {'all_errors':[]}
        for carpart in carpartErrorsHighLevel:
            for errorType in carpartErrorsHighLevel[carpart]:
                for requestFile in carpartErrorsHighLevel[carpart][errorType]['request_file']:
                    #Log the error per request file
                    #Per Request File check if request file is in output var - if yes add error to issues
                    appendedKey = ReturnJSONObjectByRequest(requestFile,outputVar) #Return the entry with request file or append new entry if request file doesn't exist and returun reference
                    if(not errorType in appendedKey['issues']): #Check for error type in issues and add
                        appendedKey['issues'][errorType] = []
                    #Check if duplicate entry or missing part and add to 
                    if(errorType == "duplicate_entry" or errorType == "missing_part"):
                        appendedKey['issues'][errorType].append(carpart)
                    elif(errorType == "missing_subparts"):
                        subpartsKey = {'carpart_id':carpart,'subparts':[]}
                        for subpart in carpartErrorsHighLevel[carpart][errorType]['subparts']:
                            subpartsKey['subparts'].append({'subpart_id':subpart})
                        appendedKey['issues'][errorType].append(subpartsKey)
                    elif(errorType == "missing_material"):
                        materialsKey = {'carpart_id':carpart,'materials':[]}
                        for mat in carpartErrorsHighLevel[carpart][errorType]['materials']:
                            materialsKey['materials'].append({'material_id':mat})
                        appendedKey['issues'][errorType].append(materialsKey)
    if(format == "csv"):
        outputVar = []
        for carpart in carpartErrorsHighLevel:
            for errorType in carpartErrorsHighLevel[carpart]:
                if(not 'materials' in carpartErrorsHighLevel[carpart][errorType]):
                    carpartErrorsHighLevel[carpart][errorType]['materials'] = None
                if(not 'subparts' in carpartErrorsHighLevel[carpart][errorType]):
                    carpartErrorsHighLevel[carpart][errorType]['subparts'] = None

                rowToAppend = {   
                    'Error Type' : errorType,  
                    'Affected Part' : carpart,
                    'Subparts Affected':carpartErrorsHighLevel[carpart][errorType]['subparts'],
                    'Materials' : carpartErrorsHighLevel[carpart][errorType]['materials'],
                    'Error Count': carpartErrorsHighLevel[carpart][errorType]['count'] #todo Add to count if error hit again
                    }
            outputVar.append(rowToAppend)
    return outputVar

def ReturnJSONObjectByRequest(requestFile, outputVar):
    if(len(outputVar['all_errors']) > 0):
        for entry in outputVar['all_errors']:
            if(entry['request_file'] == requestFile):
                return entry
    genericEntry = {'request_file':requestFile, 'issues':{}}
    outputVar['all_errors'].append(genericEntry)
    return genericEntry

def WriteJSON():
    #format Output JSON
    outputJSON = FormatOutputFor('json')
    WriteToFile("HighLevelJSONfile",outputJSON)

def WriteCSV():
    outputCSV = FormatOutputFor('csv')
    #Format Output CSV
    try:
        with open(os.path.join(OutputPath, "HighLevelCSVResults.csv"), 'w+') as csvfile:
            csv_columns = [
                'Error Type',
                'Affected Part',
                'Subparts Affected',
                'Materials',
                'Error Count'
            ]
            writer = csv.DictWriter(csvfile, fieldnames=csv_columns)
            writer.writeheader()
            for data in outputCSV:
                writer.writerow(data)
            csvfile.close()
    except IOError:
        print("I/O error")


if __name__ == "__main__":
    # Set Vendor Assets Path
    Vendor_Directory_Path = ""
    while Vendor_Directory_Path == "" or not os.path.isdir(Vendor_Directory_Path):
        Vendor_Directory_Path = raw_input("\nEnter full path to _Vendor Folder: ")

    vendorPartsPath = os.path.join(Vendor_Directory_Path, 'Parts', 'FBX')
    vendorMaterialsPath = os.path.join(Vendor_Directory_Path, 'Materials')
    #vendorTexturesPath = os.path.join(Vendor_Directory_Path, 'Textures')
    vendorMaterialsSet = GetListFromFolderOfExt(vendorMaterialsPath, '.mat')
    #vendorTextureSet = GetListFromFolderOfExt(vendorMaterialsPath, '.png')

    # Create List for Material Names # [review:charlie] What are these for? When would they be used?
    vendorPartsList = GetListFromFolderOfExt(vendorPartsPath, '.fbx')
    vendorPartsSubpartsDict = None # Will be loaded from file or generated from parts fbx folder

    # Set Requests JSON's Path to Parse
    Requests_Directory_Path = ""
    while Requests_Directory_Path == "" or not os.path.isdir(
            Requests_Directory_Path or not len(os.scandir(Requests_Directory_Path)) < 1):
        Requests_Directory_Path = raw_input("\nEnter full path to REQUESTS folder: ")
    # Set Output Path Name and validate write permission on path to generate output file
    OutputPath = ""
    while OutputPath == "" or not os.path.isdir(OutputPath) or not os.access(OutputPath, os.W_OK):
        OutputPath = raw_input("\nEnter full path for Output Folder : ")

    # Check for previous ran generated file and load from file to save time
    if(os.path.exists(os.path.join(OutputPath,"vendorPartsSubpartsDict.json"))):
        with open('vendorPartsSubpartsDict.json') as f:
            vendorPartsSubpartsDict = json.load(f)
    else:
        vendorPartsSubpartsDict = GenerateDictionaryFBXAndHierarchyFromFolder(vendorPartsPath)
        WriteToFile("vendorPartsSubpartsDict.json",vendorPartsSubpartsDict) # Write the vendor fbx hierarchy data to file to save time running multiple tests. Initial object build takes 5-10 seconds per fbx
    RequestPartsSubpartsDictionary = None
    #Generate Dictionary of {Request Filename: [Carparts and subparts]}
    if(os.path.exists(os.path.join(OutputPath,"requestsParts.json"))):
        with open('requestsParts.json') as f:
            RequestPartsSubpartsDictionary = json.load(f)
    else:
        RequestPartsSubpartsDictionary = GenerateDictionaryCarpartSubpart(Requests_Directory_Path)
        WriteToFile("requestsParts.json",RequestPartsSubpartsDictionary)

    # Create JSON for secondary output
    outputJSON = {} # Output Error JSON Log
    outputCSV = [] # Output Error Log for CSV Log [Columns: List ID	Part Affected	Status	Date Reported	Date Closed	GIT Commit	Assignee	Issue Type	Step	Description	Example Request	EXT / INT / ETC	Subparts Affected
    global listID #ListID for the error number
    listID = 0
    listMissingFiles = {'all_errors':[]}
    totalFiles = len(RequestPartsSubpartsDictionary.keys())
    progressCount = 0; 
    # Scan each Request File
    for requestKey in RequestPartsSubpartsDictionary.keys():
        progressCount += 1
        progressBar(progressCount,totalFiles,"Compare Request Files to Vendor Folder")
        # Wheelset Brakes and CarGeometry
        for geoSet in RequestPartsSubpartsDictionary[requestKey].keys(): 
            carpartsCheckedSet = set()
            #Sanity Check for 'carparts' key in RequestPartsSubpartsDictionary[requestKey][geoSet]
            if ('carparts' in RequestPartsSubpartsDictionary[requestKey][geoSet]):
                #Iterate through each Carpart in GeoSet
                for carpart in RequestPartsSubpartsDictionary[requestKey][geoSet]['carparts']:
                    #Sanity Check for keys carpart (carpart name string) and subparts [array subpart objects]
                    if('carpart' and 'subparts' in carpart): # TODO Throw exception when JSON Object not formatted
                        #Check if Carpart Exists in Vendor Parts List - If not Missing Carpart
                        if(not carpart['carpart'] in carpartsCheckedSet):
                            if(not carpart['carpart'] in vendorPartsSubpartsDict.keys()):
                                #There is no carpart - Need to add to JSON and CSV Request File, GeoSet, Carpart name
                                LogError(requestKey,'missing_part',carpart)
                            else:
                                subpartsMissing = []
                                for subpart in carpart['subparts']:
                                    if(not subpart in vendorPartsSubpartsDict[carpart['carpart']]):
                                        subpartsMissing.append(subpart)
                                if(len(subpartsMissing) > 0):
                                    LogError(requestKey,'missing_subparts',carpart,subpartsMissing)
                                materialsMissing = []
                                for mat in carpart['mats']:
                                    if(not mat in vendorMaterialsSet):
                                        materialsMissing.append(mat)
                                if(len(materialsMissing) > 0):
                                    LogError(requestKey,'missing_material',carpart,mat)
                                #Time to add all errors to output JSON 
                            carpartsCheckedSet.add(carpart['carpart']);
                        else:
                            LogError(requestKey,'duplicate_entry',carpart)
            carpartsCheckedSet.clear()
    # Write the json file of missing files
    if(len(carpartErrorsHighLevel.keys())  > 0):
        #Write Json File of missing Files
        WriteJSON()
        WriteCSV()        
    else:
        print("There are no missing files ^^")
    raw_input("Press enter to exit script...")

