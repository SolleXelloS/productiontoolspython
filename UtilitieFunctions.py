import json

# Systym Utility Functions
def WriteToFile(filename, toWriteAsJSONDump):
    myfile = open(filename, 'w+')
    myfile.write(json.dumps(toWriteAsJSONDump))
    myfile.close()

# DataStructure Utilitu Functions

#   Input (Folder Path, ext) | Output Dict{filename:[Modified Date, Path to File]}
def BuildDictFromFolderWithExt(directoryPath, fileExtToScrape):
    # Check Folder Directory Exists and has files and Extention has . prefix and folder
    if (not os.path.isdir(directoryPath)):
        return "Need valid Directory input"
    if (len(os.listdir(directoryPath)) == 0):
        return "Directory has no Files or Folders"
    if (fileExtToScrape[0] != '.'):
        return "Not Valid Ext"
    outputDict = dict()

    # Iterate each folder and file in directory. Build Dictionary of files with ext and store Modified Date/Path
    for directory in directoryPath:
        if (directory.path.endswith(fileExtToScrape)):
            filename = directory.name.replace(fileExtToScrape, "")
            outputDict[filename] = [os.path.getctime(directory.path), directory.path]
        elif (os.path.isdir(directory.path)):
            print(directory.path + " is processing")
            outputDict = Merge(outputDict, BuildDictFromFolderWithExt(directory.path, fileExtToScrape))
        # else will skip any file not has fileExtToScrape and iterate through folders First in Last Out
    return outputDict

# Python code to merge dict First in Last Out using a single expression
def Merge(dict1, dict2):
    z = dict1.copy()  # start with x's keys and values
    z.update(dict2)  # modifies z with y's keys and values & returns None
    return z

