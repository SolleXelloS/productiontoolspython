from DisplayHierarchy import *
from ImportScene import DisplayMetaData
from FbxCommon import *
import fbx
from DisplayTexture import DisplayTexture

def GetHierarchyListFromFBXFile(filePath):
    # Prepare the FBX SDK.
    lSdkManager, lScene = InitializeSdkObjects()
    # Load the scene.

    # Create the required FBX SDK data structures.
    fbxManager = fbx.FbxManager.Create()

    lResult = LoadScene(lSdkManager, lScene, filePath)
    if not lResult:
        print("\n\nAn error occurred while loading the scene...")
    else:
        hierarchyList = GetHierarchyAsList(lScene)
        # print("hierarchy set = %s" % hierarchySet)
        return hierarchyList

def GetMaterialListFromFBXFile(filePath):
    # Prepare the FBX SDK.
    lSdkManager, lScene = InitializeSdkObjects()
    # Load the scene.

    # Create the required FBX SDK data structures.
    fbxManager = fbx.FbxManager.Create()

    lResult = LoadScene(lSdkManager, lScene, filePath)
    if not lResult:
        print("\n\nAn error occurred while loading the scene...")
    else:
        materialsList = GetMaterialsAsList(lScene)
        # print("hierarchy set = %s" % hierarchySet)
        return hierarchyList

def GetTexturesListFromFBXFile(filePath):
    print()
    outputList = []
    # Prepare the FBX SDK.
    lSdkManager, lScene = InitializeSdkObjects()
    # Load the scene.

    # Create the required FBX SDK data structures.
    fbxManager = fbx.FbxManager.Create()

    lResult = LoadScene(lSdkManager, lScene, filePath)
    if not lResult:
        print("\n\nAn error occurred while loading the scene...")
    else:
        lMaterialCount = 0
        lNode = None
        outputList = []

        # Get Texture Count
        textureCount = lScene.GetTextureCount()
        print("texture count = %s" % textureCount)
        for i in range(textureCount):
            lTexture = lScene.GetTexture(i)
            print('texture name = %s' % lTexture.GetFileName())
        # Iterate through each texture
        # Return list names

        # Get Geometry

        # if pGeometry:
        #     lNode = pGeometry.GetNode()
        #     if lNode:
        #         lMaterialCount = lNode.GetMaterialCount()

        # for l in range(pGeometry.GetLayerCount()):
        #     leMat = pGeometry.GetLayer(l).GetMaterials()
        #     if leMat:
        #         if leMat.GetReferenceMode() == FbxLayerElement.eIndex:
        #             #Materials are in an undefined external table
        #             continue

        #         if lMaterialCount > 0:
        #             theColor = FbxColor()
                    
        #             header = "    Materials on layer %d: " % l
        #             for lCount in range(lMaterialCount):
        #                 lMaterial = lNode.GetMaterial(lCount)
        #                 outputList.append(lMaterial.GetName()) 


def GetTexturesListFromFBXFile(filePath):
    outputList = []
    # Prepare the FBX SDK.
    lSdkManager, lScene = InitializeSdkObjects()
    # Load the scene.

    # Create the required FBX SDK data structures.
    fbxManager = fbx.FbxManager.Create()

    lResult = LoadScene(lSdkManager, lScene, filePath)
    if not lResult:
        print("\n\nAn error occurred while loading the scene...")
    else:

        lMaterialCount = 0
        lNode = None
        NodeCount = lScene.GetSrcObjectCount()
        print('Node count = %s' % NodeCount)
        for nodeIndex in range(NodeCount):
            currentNode = lScene.GetSrcObject(nodeIndex)
            pGeometry = None
            try:
                pGeometry = currentNode.GetGeometry()
                print('Current pGeometry = %s' % pGeometry)
                print('Current node = %s' % currentNode)
                print('Current node material count = %s' % currentNode.GetMaterialCount())
                if pGeometry:
                    DisplayTexture(pGeometry)
                    #TODO implement List Output
            except:
                print("No Get Geometry Function on Node")
# Test 
if __name__ == '__main__':
    try:

        import fbx
        import os
        import sys
    except ImportError:
        print("Error: module FbxCommon failed to import.\n")
        print("Copy the files located in the compatible sub-folder lib/python<version> into your python interpreter site-packages folder.")
        import platform
        if platform.system() == 'Windows' or platform.system() == 'Microsoft':
            print('For example: copy ..\\..\\lib\\Python27_x64\\* C:\\Python27\\Lib\\site-packages')
        elif platform.system() == 'Linux':
            print('For example: cp ../../lib/Python27_x64/* /usr/local/lib/python2.7/site-packages')
        elif platform.system() == 'Darwin':
            print('For example: cp ../../lib/Python27_x64/* /Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages')
        sys.exit(1)
    fbx_filepath = ""
    while fbx_filepath == "" or not os.path.exists(fbx_filepath):
        fbx_filepath = raw_input("\nEnter full path to fbxFile: ")

    #LoadFBXFile(fbx_filepath)
    GetTexturesListFromFBXFile(fbx_filepath)
    raw_input("Script Finished")