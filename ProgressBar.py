import sys

def progressBar(value, endvalue, operationType, bar_length=20):

    percent = float(value) / endvalue
    arrow = '-' * int(round(percent * bar_length)-1) + '>'
    spaces = ' ' * (bar_length - len(arrow))

    sys.stdout.write("\r{2}: [{0}] {1}%".format(arrow + spaces, int(round(percent * 100)),operationType))
    sys.stdout.flush()